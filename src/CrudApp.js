import React, { useEffect, useState } from "react";
import Cruds from "./CrudComponents/Cruds";
import { CardBody, Card, CardHeader, Table, Button } from "reactstrap";
import CrudForm from "./CrudComponents/CrudForm";

const CrudApp = props => {
  const [questions, setQuestions] = useState([]);
  const [showForm, setShowForm] = useState(false);
  //   const [toggleForm, setToggleForm] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [questionToEdit, setQuestionToEdit] = useState({});
  //   temporary save the index
  const [questionIndex, setQuestionIndex] = useState(null);

  useEffect(() => {
    // console.log("hello");
    fetch(
      "https://opentdb.com/api.php?amount=10&category=9&difficulty=easy&type=boolean"
    )
      .then(res => res.json())
      .then(res => {
        // console.log(res);
        setQuestions(res.results);
      });
  }, []);
  //   console.log(questions);

  //   functions
  const deleteQuestion = index => {
    // console.log("you are clicking, i am deleting", index);
    const newQuestion = questions.filter((question, qIndex) => {
      return qIndex !== index;
    });
    setQuestions(newQuestion);
  };

  const saveQuestion = (question, category) => {
    //   new
    let newQuestions = [];
    if (isEditing) {
      let editedQuestion = question;
      let editedCategory = category;
      if (question === "") editedQuestion = questionToEdit.question;
      if (category === "") editedCategory = questionToEdit.category;

      newQuestions = questions.map((indivQuestion, index) => {
        if (index === questionIndex) {
          return { question: editedQuestion, category: editedCategory };
        } else {
          return indivQuestion;
        }
      });
    } else {
      newQuestions = [...questions, { question: question, category: category }];
    }
    // end of new
    setIsEditing(false);
    setQuestionToEdit({});
    setQuestionIndex(null);
    setQuestions(newQuestions);
    setShowForm(false);
  };
  const editQuestion = (question, index) => {
    setShowForm(true);
    setIsEditing(true);
    setQuestionToEdit(question);
    setQuestionIndex(index);
  };

  const toggleForm = () => {
    setShowForm(false);
    setIsEditing(false);
    setQuestionToEdit({});
  };

  // end of functions

  return (
    <div className="col-lg-8 offset-lg-2">
      <Card className="my-5">
        <CardHeader className="bg-secondary">
          <h1 className="text-center text-info">Trivia Question</h1>
        </CardHeader>
        <CardHeader>
          <Button
            color="info"
            className="text-center bg-info"
            onClick={() => setShowForm(!showForm)}
          >
            Add Questions
          </Button>
          <CrudForm
            showForm={showForm}
            // toggleForm={() => setShowForm(false)}
            toggleForm={toggleForm}
            saveQuestion={saveQuestion}
            isEditing={isEditing}
            questionToEdit={questionToEdit}
          />
        </CardHeader>
        <CardBody>
          <Table>
            <thead>
              <tr>
                <td>Questions</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <Cruds
                questions={questions}
                deleteQuestion={deleteQuestion}
                // toggleForm={() => setShowForm(true)}
                toggleForm={editQuestion}
              />
            </tbody>
          </Table>
        </CardBody>
      </Card>
    </div>
  );
};
export default CrudApp;
