import React, { Component } from "react";
import Button from "./Button";

class Buttons extends Component {
  render() {
    return (
      <div
        className="d-flex justify-content-around"
        style={{ margin: "0 200px" }}
      >
        <Button
          color={"btn-info"}
          handleOnClick={this.props.handlePlus}
          text={"+ 1"}
        />
        <Button
          color={"btn-warning"}
          text={"- 1"}
          handleOnClick={this.props.handleMinus}
        />
        <Button
          color={"btn-success"}
          text={"Reset"}
          handleOnClick={this.props.handleReset}
        />
        <Button
          color={"btn-secondary"}
          text={"x 2"}
          handleOnClick={this.props.handleMultiply}
        />
      </div>
    );
  }
}
export default Buttons;
