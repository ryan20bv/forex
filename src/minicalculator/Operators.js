import React from "react";

import Button from "./Button";

const Operators = props => {
  return (
    <div
      className="d-flex justify-content-around px-3"
      style={{ margin: "0 200px" }}
    >
      <Button
        text={"+ "}
        handleOnClick={props.handlePlus}
        design={"btn-info rounded-circle m-2 border border-dark"}
        style={{
          width: "50px",
          height: "50px"
        }}
      />
      <Button
        text={"- "}
        handleOnClick={props.handleMinus}
        design={"btn-success rounded-circle m-2 border border-dark"}
        style={{
          width: "50px",
          height: "50px"
        }}
      />
      <Button
        text={"x"}
        handleOnClick={props.handleMultiply}
        design={"btn-warning rounded-circle m-2 border border-dark"}
        style={{
          width: "50px",
          height: "50px"
        }}
      />
      <Button
        text={"/"}
        handleOnClick={props.handleDivide}
        design={"btn-secondary rounded-circle m-2 border border-dark"}
        style={{
          width: "50px",
          height: "50px"
        }}
      />

      <span>{props.text}</span>
    </div>
  );
};

export default Operators;
