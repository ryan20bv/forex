import React from "react";
import { FormGroup, Label, Input } from "reactstrap";

const FirstInput = props => {
  return (
    <FormGroup>
      <Label>First Input:</Label>
      <Input
        placeholder={props.FirstInput}
        onChange={props.onChange}
        type="number"
      />
      {props.text}
    </FormGroup>
  );
};

export default FirstInput;
