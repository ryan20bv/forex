import React from "react";

const NumButton = props => {
  return (
    <button
      className={props.design}
      style={props.style}
      //   onClick={props.handleOnClick}
      onClick={() => {
        props.onClick(props.text);
      }}
    >
      {props.text}
    </button>
  );
};

export default NumButton;
