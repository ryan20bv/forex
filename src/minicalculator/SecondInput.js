import React from "react";
import { FormGroup, Label, Input } from "reactstrap";

const SecondInput = props => {
  return (
    <FormGroup>
      <Label>Second Input:</Label>
      <Input
        placeholder={props.SecondInput}
        onChange={props.onChange}
        type="number"
      />
    </FormGroup>
  );
};

export default SecondInput;
