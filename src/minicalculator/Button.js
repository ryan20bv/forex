import React from "react";

const Button = props => {
  return (
    <button
      className={props.design}
      style={props.style}
      onClick={props.handleOnClick}
    >
      {props.text}
    </button>
  );
};

export default Button;
