import React from "react";
import NumButton from "./NumButton";

const Numbers = props => {
  return (
    <div>
      <div>
        <NumButton
          text={7}
          design={"btn-secondary rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.addToInput}
        />
        <NumButton
          text={8}
          design={"btn-light rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.addToInput}
        />
        <NumButton
          text={9}
          design={"btn-secondary rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.addToInput}
        />
        <NumButton
          text={"/"}
          design={"btn-danger rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.divide}
        />
      </div>
      <div>
        <NumButton
          text={4}
          design={"btn-light rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.addToInput}
        />
        <NumButton
          text={5}
          design={"btn-secondary rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.addToInput}
        />
        <NumButton
          text={6}
          design={"btn-light rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.addToInput}
        />
        <NumButton
          text={"X"}
          design={"btn-danger rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.multiply}
        />
      </div>
      <div>
        <NumButton
          text={1}
          design={"btn-secondary rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.addToInput}
        />
        <NumButton
          text={2}
          design={"btn-light rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.addToInput}
        />
        <NumButton
          text={3}
          design={"btn-secondary rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.addToInput}
        />
        <NumButton
          text={"-"}
          design={"btn-danger rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.subtract}
        />
      </div>
      <div>
        <NumButton
          text={0}
          design={"btn-info rounded-pill m-2 border border-dark"}
          style={{
            width: "165px",
            height: "75px"
          }}
          onClick={props.addZero}
        />

        <NumButton
          text={"."}
          design={"btn-warning rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.addDecimal}
        />
        <NumButton
          text={"+"}
          design={"btn-danger rounded-circle m-2 border border-dark"}
          style={{
            width: "75px",
            height: "75px"
          }}
          onClick={props.add}
        />
      </div>
      <div>
        <button
          className="btn-primary rounded-pill m-2 border border-dark"
          style={{
            width: "160px",
            height: "50px"
          }}
          onClick={props.clear}
        >
          Clear
        </button>
        {/* <NumButton
          text={"Clear"}
          design={"btn-primary rounded-pill m-2 border border-dark"}
          style={{
            width: "160px",
            height: "50px"
          }}
        /> */}

        <NumButton
          text={"="}
          design={"btn-danger rounded-pill m-2 border border-dark"}
          style={{
            width: "160px",
            height: "50px"
          }}
          onClick={props.equals}
        />
      </div>
    </div>
  );
};
export default Numbers;
