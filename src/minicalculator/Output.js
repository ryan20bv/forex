import React from "react";
import { FormGroup, Label, Input } from "reactstrap";
const Output = props => {
  return (
    <FormGroup>
      <Label>Output:</Label>
      <Input placeholder={props.output} />
      {/* <h1>{props.output}</h1> */}
    </FormGroup>
  );
};

export default Output;
