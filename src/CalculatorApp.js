import React, { useState } from "react";
import FirstInput from "./minicalculator/FirstInput";
import Operators from "./minicalculator/Operators";
import SecondInput from "./minicalculator/SecondInput";
import Output from "./minicalculator/Output";
import Numbers from "./minicalculator/Numbers";

const CalculatorApp = props => {
  const [firstInput, setFirstInput] = useState("");
  const [secondInput, setSecondInput] = useState("");
  const [output, setOutput] = useState("");
  const [operator, setOperator] = useState("");
  const [text, setText] = useState();
  const [input, setInput] = useState("");
  const [entered, setEntered] = useState("");
  let [currentNumber, setCurrentNumber] = useState("");
  const [value, setValue] = useState("");

  const handleFirstInput = e => {
    // console.log(firstInput);
    setOutput();
    setFirstInput(e.target.value);
  };
  const handleSecondInput = e => {
    setOutput();
    setSecondInput(e.target.value);
  };

  const handlePlus = () => {
    setOperator("add");
    setText("+");
  };

  const handleMinus = () => {
    setOperator("minus");
    setText("-");
  };
  const handleDivide = () => {
    setOperator("divide");
    setText("/");
  };
  const handleMultiply = () => {
    setOperator("multiply");
    setText("*");
  };
  const handleOutput = () => {
    if (text == null) {
      alert("choose Operator");
    } else {
      if (operator === "add") {
        let newOutput = parseInt(firstInput) + parseInt(secondInput);
        setOutput(newOutput);
      }
      if (operator === "minus") {
        let newOutput = parseInt(firstInput) - parseInt(secondInput);
        setOutput(newOutput);
      }
      if (operator === "multiply") {
        let newOutput = parseInt(firstInput) * parseInt(secondInput);
        setOutput(newOutput);
      }
      if (operator === "divide") {
        let newOutput = parseFloat(firstInput) / parseFloat(secondInput);
        setOutput(newOutput);
      }
    }
  };
  // second calculator functions

  const addToInput = val => {
    setInput(input + val);
  };

  const addDecimal = val => {
    if (input.indexOf(".") === -1) {
      setInput(input + val);
    }
  };

  const addZero = val => {
    if (input !== "") {
      setInput(input + val);
    }
  };

  const clear = () => {
    setInput("");
    setEntered("");
    setText("");
  };

  const add = () => {
    setEntered(input);
    setInput("");
    setOperator("add");
    setText("+");
  };
  const divide = () => {
    setEntered(input);
    setInput("");
    setOperator("divide");
    setText("/");
  };
  const subtract = () => {
    setEntered(input);
    setInput("");
    setOperator("subtract");
    setText("-");
  };
  const multiply = () => {
    setEntered(input);
    setInput("");
    setOperator("multiply");
    setText("*");
  };

  const equals = () => {
    currentNumber = input;
    if (operator === "add") {
      let result = parseFloat(currentNumber) + parseFloat(entered);
      let res = result.toString();
      setInput(res);
    } else if (operator === "subtract") {
      let result = parseFloat(currentNumber) - parseFloat(entered);
      let res = result.toString();
      setInput(res);
    } else if (operator === "multiply") {
      let result = parseFloat(currentNumber) * parseFloat(entered);
      let res = result.toString();
      setInput(res);
    } else if (operator === "divide") {
      let result = parseFloat(currentNumber) / parseFloat(entered);
      let res = result.toString();
      setInput(res);
    }
  };

  return (
    <React.Fragment>
      <div>
        <h1 className="text-center">this is mini calculator</h1>
        <div className="d-flex justify-content-around align-items-center my-3">
          <div
            style={{
              width: "500px",
              height: "600px",
              border: "5px solid black"
            }}
            className="d-flex justify-content-center align-items-center flex-column"
          >
            <FirstInput onChange={handleFirstInput} text={text} />

            <Operators
              handlePlus={handlePlus}
              handleMinus={handleMinus}
              handleMultiply={handleMultiply}
              handleDivide={handleDivide}
              operator={operator}
            />
            <SecondInput onChange={handleSecondInput} />

            <button
              onClick={handleOutput}
              className="rounded-pill border btn-primary"
              style={{
                width: "100px",
                height: "50px"
              }}
            >
              <h1>=</h1>
            </button>

            <Output output={output} />
          </div>
          {/* second calculator */}
          <div
            style={{
              width: "500px",
              height: "600px",
              border: "5px solid black"
            }}
            className="d-flex align-items-center flex-column bg-dark py-5"
          >
            <div
              style={{
                width: "400px",
                height: "100px",
                border: "5px solid black"
              }}
              className="d-flex justify-content-center align-items-left flex-column bg-light px-3"
            >
              <h3 className="text-right">
                {entered} {text}
              </h3>
              <h1 className="text-right">{input === "" ? "0" : input}</h1>
            </div>
            <Numbers
              addToInput={addToInput}
              addDecimal={addDecimal}
              clear={clear}
              addZero={addZero}
              add={add}
              divide={divide}
              multiply={multiply}
              subtract={subtract}
              equals={equals}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export default CalculatorApp;
