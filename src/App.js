// import React from "react";

// function App() {
//   return <h1 className="text-center">Hello Batch 55</h1>;
// }
// export default App;

// transfrom into class base

import React, { Component } from "react";
import Box from "./components/Box";
import Buttons from "./components/Buttons";

class App extends Component {
  state = {
    count: 1
  };

  handlePlus = () => {
    let newCount = this.state.count + 1;

    this.setState({ count: newCount });
  };

  handleMinus = () => {
    let newCount = this.state.count - 1;
    this.setState({ count: newCount });
  };
  handleReset = () => {
    this.setState({ count: 0 });
  };
  handleMultiply = () => {
    this.setState({ count: this.state.count * 2 });
  };
  render() {
    return (
      <>
        <Box count={this.state.count} />
        <Buttons
          handlePlus={this.handlePlus}
          handleMinus={this.handleMinus}
          handleReset={this.handleReset}
          handleMultiply={this.handleMultiply}
        />
      </>
    );
  }
}
export default App;
