import React from "react";
import Forex from "./forexComponents/Forex";

const ForexApp = () => {
  return (
    <div className="bg-secondary d-flex justify-content-center align-items-center min-vh-100">
      <Forex />
    </div>
  );
};

// class ForexApp extends Component {
//   render() {
//     return (
//       <div className="bg-secondary d-flex justify-content-center align-items-center vh-100">
//         <Forex />
//       </div>
//     );
//   }
// }
export default ForexApp;
