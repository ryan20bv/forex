import React from "react";
import { Button } from "reactstrap";

const Cruds = props => {
  return (
    <React.Fragment>
      {props.questions.map((question, index) => (
        <tr key={index}>
          <td>
            <p>Question #{index + 1}</p>
            <p className="text-danger">{question.question} </p>
            <p className="text-secondary">{question.category} </p>
          </td>
          <td>
            <Button
              className="my-3"
              color="danger"
              onClick={() => props.deleteQuestion(index)}
            >
              Delete
            </Button>
            <Button
              color="info"
              onClick={() => props.toggleForm(question, index)}
            >
              Edit
            </Button>
          </td>
        </tr>
      ))}
    </React.Fragment>
  );
};
export default Cruds;
