import React, { useState } from "react";
import ForexDropdown from "./ForexDropdown";
import ForexInput from "./ForexInput";
import { Button } from "reactstrap";
import Rates from "./AllRates";

const Forex = props => {
  const [amount, setAmount] = useState(0);
  const [baseCurrency, setBaseCurrency] = useState("");
  const [targetCurrency, setTargetCurrency] = useState("");
  const [convertedAmount, setConvertedAmount] = useState(0);
  const [rate, setRate] = useState(0);
  const [targetCode, setTargetCode] = useState("");
  const [baseCode, setBaseCode] = useState("");
  const [allRates, setAllRates] = useState({});
  const [error, setError] = useState("");
  const [code, setCode] = useState("");
  const [showTable, setShowTable] = useState(false);

  const handleAmount = e => {
    e.target.value <= 0 || e.target.value == null
      ? alert("Invalid input")
      : setAmount(e.target.value);
  };

  const handleBaseCurrency = currency => {
    const code = baseCurrency;
    // const code = currency.code;

    // setTargetCurrency(currency.code);

    fetch("https://api.exchangeratesapi.io/latest?base=" + code)
      .then(res => res.json())
      .then(res => {
        const ratesArray = Object.entries(res.allRates);
        setAllRates(ratesArray);
        setShowTable(true);
      });
    setBaseCurrency(currency);
    // setCode(currency.code);
  };

  const handleTargetCurrency = currency => {
    setTargetCurrency(currency);
  };

  const handleConvert = () => {
    if (targetCurrency != null && baseCurrency != null && amount > 0) {
      setError("");
      const code = baseCurrency.code;

      fetch("https://api.exchangeratesapi.io/latest?base=" + code)
        .then(res => res.json())
        .then(res => {
          // console.log(res);
          const allRates = res.rates;
          const targetCode = targetCurrency.code;
          const rate = res.rates[targetCode];
          // console.log(rate);
          setConvertedAmount(amount * rate);
          setRate(rate);
          setTargetCode(targetCode);
          setBaseCode(code);
          setAllRates(allRates);
        });
    } else {
      setError("Fill up the form properly");
    }
  };

  // console.log(this.state.amount);
  // console.log(this.state.allRate);
  return (
    <div style={{ width: "70%" }}>
      <h1 className="text-center my-5">Forex Calculator</h1>
      <div
        className="d-flex justify-content-around"
        style={{ margin: "0 200px" }}
      >
        <ForexDropdown
          label={"Base Currency"}
          onClick={handleBaseCurrency}
          currency={baseCurrency}
        />
        <ForexDropdown
          label={"Target Currency"}
          onClick={handleTargetCurrency}
          currency={targetCurrency}
        />
      </div>
      <div className="d-flex justify-content-around">
        <ForexInput
          label={"Amount"}
          placeholder={"Amount to convert"}
          onChange={handleAmount}
        />
        {/* <Button color="info" onCLick={this.handeConvert}>
              Convert
            </Button> */}
        <button className="btn btn-info" onClick={handleConvert}>
          Convert
        </button>
      </div>
      <div>
        {showTable === true ? (
          <div>
            <h1 className="text-center">
              Exchange Rate: {baseCode} {rate} {targetCode}
            </h1>
          </div>
        ) : (
          ""
        )}
        <h1 className="text-center">
          {convertedAmount} <span>{targetCode}</span>
        </h1>
        <h1 className="text-center text-color-red">{error}</h1>
      </div>
      {showTable === true ? (
        <div className="row">
          <div className="col-lg-12">
            <div className="d-flex flex-wrap w-100 align-items-center justify-content-around mt-3">
              <Rates allRates={allRates} />
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

// class Forex extends Component {
//   state = {
//     amount: 0,
//     baseCurrency: "",
//     targetCurrency: null,
//     convertedAmount: 0,
//     rate: 0,
//     targetCode: null,
//     baseCode: null,
//     allRates: "",
//     error: "",
//     code: ""
//   };

//   handleAmount = e => {
//     e.target.value <= 0 || e.target.value == null
//       ? alert("Invalid input")
//       : this.setState({ amount: e.target.value });
//   };

//   handleBaseCurrency = currency => {
//     this.setState({ baseCurrency: currency });
//     this.setState({ code: currency.code });
//     const code = this.state.code;

//     fetch("https://api.exchangeratesapi.io/latest?base=" + code)
//       .then(res => res.json())
//       .then(res => {
//         const allRates = res.rates;
//         this.setState({ allRates: allRates });
//       });
//   };

//   handleTargetCurrency = currency => {
//     this.setState({ targetCurrency: currency });
//   };

//   handleConvert = () => {
//     if (
//       this.state.targetCurrency != null &&
//       this.state.baseCurrency != null &&
//       this.state.amount > 0
//     ) {
//       this.setState({ error: "" });
//       const code = this.state.baseCurrency.code;

//       fetch("https://api.exchangeratesapi.io/latest?base=" + code)
//         .then(res => res.json())
//         .then(res => {
//           // console.log(res);
//           const allRates = res.rates;
//           const targetCode = this.state.targetCurrency.code;
//           const rate = res.rates[targetCode];
//           // console.log(rate);
//           this.setState({ convertedAmount: this.state.amount * rate });
//           this.setState({ rate: rate });
//           this.setState({ targetCode: targetCode });
//           this.setState({ baseCode: code });
//           this.setState({ allRate: allRates });
//         });
//     } else {
//       this.setState({ error: "Fill up the form properly" });
//     }
//   };

//   render() {
//     // console.log(this.state.amount);
//     // console.log(this.state.allRate);
//     return (
//       <div style={{ width: "70%" }}>
//         <h1 className="text-center my-5">Forex Calculator</h1>
//         <div
//           className="d-flex justify-content-around"
//           style={{ margin: "0 200px" }}
//         >
//           <ForexDropdown
//             label={"Base Currency"}
//             onClick={this.handleBaseCurrency}
//             currency={this.state.baseCurrency}
//           />
//           <ForexDropdown
//             label={"Target Currency"}
//             onClick={this.handleTargetCurrency}
//             currency={this.state.targetCurrency}
//           />
//         </div>
//         <div className="d-flex justify-content-around">
//           <ForexInput
//             label={"Amount"}
//             placeholder={"Amount to convert"}
//             onChange={this.handleAmount}
//           />
//           {/* <Button color="info" onCLick={this.handeConvert}>
//             Convert
//           </Button> */}
//           <button className="btn btn-info" onClick={this.handleConvert}>
//             Convert
//           </button>
//         </div>
//         <div>
//           <h1 className="text-center">
//             Exchange Rate: {this.state.baseCode} {this.state.rate}{" "}
//             {this.state.targetCode}
//           </h1>
//           <h1 className="text-center">
//             {this.state.convertedAmount} <span>{this.state.targetCode}</span>
//           </h1>
//           <h1 className="text-center text-color-red">{this.state.error}</h1>
//         </div>
//         <div className="row">
//           <div className="col-lg-12">
//             <div className="d-flex flex-wrap w-100 align-items-center justify-content-around mt-3">
//               <Rates allRates={this.state.allRates} />
//             </div>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }
export default Forex;
