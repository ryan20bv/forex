import React, { useState } from "react";
import {
  FormGroup,
  Label,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import Currencies from "./ForexData";

function ForexDropdown(props) {
  //useState is a react hook
  const [dropdownIsOpen, setDropdownIsOpen] = useState(false);
  const [currency, setCurrency] = useState(null);
  //   state = {
  //     dropdownIsOpen: false,
  //     currency: null
  //   };

  // console.log(Currencies);
  return (
    <FormGroup>
      <Label>{props.label}</Label>
      <Dropdown
        isOpen={dropdownIsOpen}
        toggle={() => setDropdownIsOpen(!dropdownIsOpen)}
      >
        <DropdownToggle caret>
          {!props.currency ? "Choose Currency" : props.currency.currency}
        </DropdownToggle>
        <DropdownMenu>
          {Currencies.map((indivcurrency, index) => (
            <DropdownItem
              key={index}
              onClick={() => props.onClick(indivcurrency)}
            >
              {indivcurrency.currency}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </Dropdown>
    </FormGroup>
  );
}
export default ForexDropdown;
