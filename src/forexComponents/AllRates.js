import React from "react";
import { Table } from "reactstrap";

const Rates = props => {
  return (
    <Table className="striped border">
      <thead>
        <tr>
          <th>Currency</th>
          <th>Rate</th>
        </tr>
      </thead>
      <tbody>
        {props.allRates.map((allRate, index) => (
          <tr key={index}>
            <td>{allRate[0]}</td>
            <td>{allRate[1]}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

// class Rates extends Component {
//   render() {
//     const { allRates } = this.props;
//     return (
//       <React.Fragment>
//         {Object.keys(allRates).map(key => (
//           <span className=" px-2 m-2" key={key}>
//             {key} - {allRates[key]}
//           </span>
//         ))}
//       </React.Fragment>
//     );
//   }
// }

export default Rates;
